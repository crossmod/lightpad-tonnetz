/*
<metadata description="Travel across the tonnetz">
    <groups>

    </groups>
    <variables>
    <variable name="bgColour" displayName="Background colour" type="colour" value="0xFF0000" />
    <variable name="pressureColour" displayName="Pressure colour" type="colour" value="0xff00ff" />
    <variable name="arrowColour" displayName="Arrow colour" type="colour" value="0x00ff22" />
    <variable name="majorColour" displayName="Major colour" type="colour" value="0xFF2200" />
    <variable name="minorColour" displayName="Minor colour" type="colour" value="0x0088ff" />
    <variable name="flatColour" displayName="Flat colour" type="colour" value="0xFFFF00" />
    <variable name="octaveColour" displayName="Octave colour" type="colour" value="0xFFFF00" />
    </variables>
</metadata>
*/


// root=0, third=1, fifth=2
int klang[3];
int newKlang[3];
bool major;
bool sideHeld;

void initialise()
{
    // initialise to C major
    klang[0] = 60;
    klang[1] = 64;
    klang[2] = 67;
    major = true;
    sideHeld = false;
}


void touchStart (int index, float x, float y, float z, float vz)
{
    addPressurePoint (pressureColour, x, y, z * 10);
    int ix, iy;   
    ix = int((x/2)*15);
    iy = int((y/2)*15);
    //log(ix);
    //log(iy);
    
    //return;
    // left column
    if(ix<5) {
        if(iy<5)
            handleTopLeft();
        if(iy>=5 && iy<10)
            handleMiddleLeft();
        if(iy>=10)
            handleBottomLeft();
    }
    
    // middle column
    else if(ix>=5 && ix<10) {    
        if(iy<5)
            handleTopMiddle();
        if(iy>=5 && iy<10)
            handleMiddle();
        if(iy>=10)
            handleBottomMiddle();
    }
    
    // right column
    else if(ix>10) {
        if(iy<5)
            handleTopRight();
        if(iy>=5 && iy<10)
            handleMiddleRight();
        if(iy>=10)
            handleBottomRight();
    }
}

void touchMove (int index, float x, float y, float z, float vz)
{
    addPressurePoint (pressureColour, x, y, z * 10);
}

void touchEnd (int index, float x, float y, float z, float vz)
{
    int ix, iy;   
    ix = int((x/2)*15);
    iy = int((y/2)*15);
    
    // middle button
    if(ix>=5 && ix<10 && iy>=5 && iy<10) {
        stopAllNotes();
    }
    // either of the side buttons
    if((ix<5 || ix>=10) && iy>=5 && iy<10) {
        log(-1);
        sideHeld = false;
    }
}

void stopAllNotes()
{
    // turn off every note
    for(int i=0; i<128; i++) {
        sendNoteOff(0, i, 64);
    }
}

void swapKlang()
{
    klang[0] = newKlang[0];
    klang[1] = newKlang[1];
    klang[2] = newKlang[2];
    major = !major;
}

// -------------------------------
// Note name
// -------------------------------
void handleMiddle()
{
    log(klang[0]);
    log(klang[1]);
    log(klang[2]);
    
    stopAllNotes();
    
    sendNoteOn(0, klang[0], 64);
    sendNoteOn(0, klang[1], 64);
    sendNoteOn(0, klang[2], 64);
}

// -------------------------------
// Navigation arrows
// -------------------------------
void handleTopLeft()
{
    if(major)
        return;
    // minor L transformation
    newKlang[0] = klang[2]+1;
    newKlang[1] = klang[0];
    newKlang[2] = klang[1];
    swapKlang();
}

void handleBottomLeft()
{
    if(!major)
        return;
    // major R transformation
    newKlang[0] = klang[2]+2;
    newKlang[1] = klang[0];
    newKlang[2] = klang[1];
    swapKlang();
}
void handleTopMiddle()
{
    if(!major)
        return;
    // major P transformation
    newKlang[0] = klang[0];
    newKlang[1] = klang[1]-1;
    newKlang[2] = klang[2];
    swapKlang();
}

void handleBottomMiddle()
{
    if(major)
        return;
    // minor P transformation
    newKlang[0] = klang[0];
    newKlang[1] = klang[1]+1;
    newKlang[2] = klang[2];
    swapKlang();
}
void handleTopRight()
{
    if(major)
        return;
    // minor R transformation
    newKlang[0] = klang[1];
    newKlang[1] = klang[2];
    newKlang[2] = klang[0]-2;
    swapKlang();
}
void handleBottomRight()
{
    if(!major)
        return;
    // major L transformation
    newKlang[0] = klang[1];
    newKlang[1] = klang[2];
    newKlang[2] = klang[0]-1;
    swapKlang();
}

// -------------------------------
// Side buttons
// -------------------------------
void resetKlang() {
    klang[0] = 60;
    klang[1] = 64;
    klang[2] = 67;
    major = true;
}

void handleMiddleLeft()
{
    if(sideHeld) {
        resetKlang();
        return;
    }
    sideHeld = true;
    // shift octave down
    klang[0] = klang[0]-12;
    klang[1] = klang[1]-12;
    klang[2] = klang[2]-12;
}
void handleMiddleRight()
{
    if(sideHeld) {
        resetKlang();
        return;
    }
    sideHeld = true;
    // shift octave up
    klang[0] = klang[0]+12;
    klang[1] = klang[1]+12;
    klang[2] = klang[2]+12;
}

// -------------------------------
// Drawing functions
// -------------------------------

void repaint()
{
    clearDisplay();
    
    if(major) {
        drawUpArrow();
        drawDownRightArrow();
        drawDownLeftArrow();
    } else {
        drawDownArrow();
        drawUpRightArrow();
        drawUpLeftArrow();
    }        
    drawNoteLetter();
    drawOctaveDown();
    drawOctaveUp();
    
    drawPressureMap();
    fadePressureMap();
}

void drawOctaveDown()
{
    fillPixel(octaveColour, 0, 7);
    fillPixel(octaveColour, 1, 7);
    fillPixel(octaveColour, 2, 7);
}

void drawOctaveUp()
{
    fillPixel(octaveColour, 14, 7);
    fillPixel(octaveColour, 13, 7);
    fillPixel(octaveColour, 12, 7);
    fillPixel(octaveColour, 13, 6);
    fillPixel(octaveColour, 13, 8);
}

void drawNoteLetter()
{
    int noteIndex = klang[0] % 12;
    int noteColour;
    if(major)
        noteColour = majorColour;
    else
        noteColour = minorColour;
    
    if(noteIndex == 0) {
        drawC(noteColour);
    }
    if(noteIndex == 1) {
        drawD(noteColour);
        drawFlat();
    }
    if(noteIndex == 2) {
        drawD(noteColour);
    }
    if(noteIndex == 3) {
        drawE(noteColour);
        drawFlat();
    }
    if(noteIndex == 4) {
        drawE(noteColour);
    }
    if(noteIndex == 5) {
        drawF(noteColour);
    }
    if(noteIndex == 6) {
        drawG(noteColour);
        drawFlat();
    }
    if(noteIndex == 7) {
        drawG(noteColour);
    }
    if(noteIndex == 8) {
        drawA(noteColour);
        drawFlat();
    }
    if(noteIndex == 9) {
        drawA(noteColour);
    }
    if(noteIndex == 10) {
        drawB(noteColour);
        drawFlat();
    }
    if(noteIndex == 11) {
        drawB(noteColour);
    }
}

void drawA(int colour)
{
    fillPixel(colour, 5, 5);
    fillPixel(colour, 5, 6);
    fillPixel(colour, 5, 7);
    fillPixel(colour, 5, 8);
    fillPixel(colour, 5, 9);
    fillPixel(colour, 8, 5);
    fillPixel(colour, 8, 6);
    fillPixel(colour, 8, 7);
    fillPixel(colour, 8, 8);
    fillPixel(colour, 8, 9);
    fillPixel(colour, 6, 5);
    fillPixel(colour, 7, 5);
    fillPixel(colour, 6, 7);
    fillPixel(colour, 7, 7);
}

void drawB(int colour)
{
    fillPixel(colour, 5, 5);
    fillPixel(colour, 5, 6);
    fillPixel(colour, 5, 7);
    fillPixel(colour, 5, 8);
    fillPixel(colour, 5, 9);
    fillPixel(colour, 8, 7);
    fillPixel(colour, 8, 8);
    fillPixel(colour, 8, 9);
    fillPixel(colour, 6, 5);
    fillPixel(colour, 7, 5);
    fillPixel(colour, 6, 7);
    fillPixel(colour, 7, 7);
    fillPixel(colour, 6, 9);
    fillPixel(colour, 7, 9);
    fillPixel(colour, 7, 6);
}

void drawC(int colour)
{
    fillPixel(colour, 5, 5);
    fillPixel(colour, 5, 6);
    fillPixel(colour, 5, 7);
    fillPixel(colour, 5, 8);
    fillPixel(colour, 5, 9);
    fillPixel(colour, 6, 5);
    fillPixel(colour, 7, 5);
    fillPixel(colour, 8, 5);
    fillPixel(colour, 6, 9);
    fillPixel(colour, 7, 9);
    fillPixel(colour, 8, 9);
}

void drawD(int colour)
{
    fillPixel(colour, 5, 5);
    fillPixel(colour, 5, 6);
    fillPixel(colour, 5, 7);
    fillPixel(colour, 5, 8);
    fillPixel(colour, 5, 9);
    fillPixel(colour, 6, 5);
    fillPixel(colour, 7, 5);
    fillPixel(colour, 6, 9);
    fillPixel(colour, 7, 9);
    fillPixel(colour, 8, 6);
    fillPixel(colour, 8, 7);
    fillPixel(colour, 8, 8);
}

void drawE(int colour)
{
    fillPixel(colour, 5, 5);
    fillPixel(colour, 5, 6);
    fillPixel(colour, 5, 7);
    fillPixel(colour, 5, 8);
    fillPixel(colour, 5, 9);
    fillPixel(colour, 6, 5);
    fillPixel(colour, 7, 5);
    fillPixel(colour, 8, 5);
    fillPixel(colour, 6, 9);
    fillPixel(colour, 7, 9);
    fillPixel(colour, 8, 9);
    fillPixel(colour, 6, 7);
    fillPixel(colour, 7, 7);
}

void drawF(int colour)
{
    fillPixel(colour, 5, 5);
    fillPixel(colour, 5, 6);
    fillPixel(colour, 5, 7);
    fillPixel(colour, 5, 8);
    fillPixel(colour, 5, 9);
    fillPixel(colour, 6, 5);
    fillPixel(colour, 7, 5);
    fillPixel(colour, 8, 5);
    fillPixel(colour, 6, 7);
    fillPixel(colour, 7, 7);
}

void drawG(int colour)
{
    fillPixel(colour, 5, 5);
    fillPixel(colour, 5, 6);
    fillPixel(colour, 5, 7);
    fillPixel(colour, 5, 8);
    fillPixel(colour, 5, 9);
    fillPixel(colour, 6, 5);
    fillPixel(colour, 7, 5);
    fillPixel(colour, 8, 5);
    fillPixel(colour, 6, 9);
    fillPixel(colour, 7, 9);
    fillPixel(colour, 8, 9);
    fillPixel(colour, 8, 8);
    fillPixel(colour, 8, 7);
    fillPixel(colour, 7, 7);
}

void drawFlat()
{
    fillPixel(flatColour, 9, 9);
}

void drawUpArrow()
{
    fillPixel(arrowColour, 7, 0);
    fillPixel(arrowColour, 7, 1);
    fillPixel(arrowColour, 7, 2);
    fillPixel(arrowColour, 7, 3);
    fillPixel(arrowColour, 7, 4);
    fillPixel(arrowColour, 6, 1);
    fillPixel(arrowColour, 5, 2);
    fillPixel(arrowColour, 8, 1);
    fillPixel(arrowColour, 9, 2);
}

void drawDownArrow()
{
    fillPixel(arrowColour, 7, 10);
    fillPixel(arrowColour, 7, 11);
    fillPixel(arrowColour, 7, 12);
    fillPixel(arrowColour, 7, 13);
    fillPixel(arrowColour, 7, 14);
    fillPixel(arrowColour, 5, 12);
    fillPixel(arrowColour, 6, 13);
    fillPixel(arrowColour, 8, 13);
    fillPixel(arrowColour, 9, 12);
}

void drawUpRightArrow()
{
    fillPixel(arrowColour, 11, 0);
    fillPixel(arrowColour, 12, 0);
    fillPixel(arrowColour, 13, 0);
    fillPixel(arrowColour, 14, 0);
    fillPixel(arrowColour, 14, 1);
    fillPixel(arrowColour, 14, 2);
    fillPixel(arrowColour, 14, 3);
    fillPixel(arrowColour, 13, 1);
    fillPixel(arrowColour, 12, 2);
    fillPixel(arrowColour, 11, 3);
    fillPixel(arrowColour, 10, 4);
}

void drawUpLeftArrow()
{
    fillPixel(arrowColour, 0, 0);
    fillPixel(arrowColour, 0, 1);
    fillPixel(arrowColour, 0, 2);
    fillPixel(arrowColour, 0, 3);
    fillPixel(arrowColour, 1, 0);
    fillPixel(arrowColour, 2, 0);
    fillPixel(arrowColour, 3, 0);
    fillPixel(arrowColour, 1, 1);
    fillPixel(arrowColour, 2, 2);
    fillPixel(arrowColour, 3, 3);
    fillPixel(arrowColour, 4, 4);
}

void drawDownRightArrow()
{
    fillPixel(arrowColour, 14, 14);
    fillPixel(arrowColour, 14, 13);
    fillPixel(arrowColour, 14, 12);
    fillPixel(arrowColour, 14, 11);
    fillPixel(arrowColour, 13, 14);
    fillPixel(arrowColour, 12, 14);
    fillPixel(arrowColour, 11, 14);
    fillPixel(arrowColour, 13, 13);
    fillPixel(arrowColour, 12, 12);
    fillPixel(arrowColour, 11, 11);
    fillPixel(arrowColour, 10, 10);
}

void drawDownLeftArrow()
{
    fillPixel(arrowColour, 0, 14);
    fillPixel(arrowColour, 0, 13);
    fillPixel(arrowColour, 0, 12);
    fillPixel(arrowColour, 0, 11);
    fillPixel(arrowColour, 1, 14);
    fillPixel(arrowColour, 2, 14);
    fillPixel(arrowColour, 3, 14);
    fillPixel(arrowColour, 1, 13);
    fillPixel(arrowColour, 2, 12);
    fillPixel(arrowColour, 3, 11);
    fillPixel(arrowColour, 4, 10);
}


/*
<display backgroundColour="0xFFFF6565" textColour ="0xFFFFFFFF">
    <pixels>
        <pixel index="0" colour="0xFF65FF74" />
        <pixel index="1" colour="0xFF65FF74" />
        <pixel index="2" colour="0xFF65FF74" />
        <pixel index="3" colour="0xFF65FF74" />
        <pixel index="11" colour="0xFF65FF74" />
        <pixel index="12" colour="0xFF65FF74" />
        <pixel index="13" colour="0xFF65FF74" />
        <pixel index="14" colour="0xFF65FF74" />
        <pixel index="15" colour="0xFF65FF74" />
        <pixel index="16" colour="0xFF65FF74" />
        <pixel index="28" colour="0xFF65FF74" />
        <pixel index="29" colour="0xFF65FF74" />
        <pixel index="30" colour="0xFF65FF74" />
        <pixel index="32" colour="0xFF65FF74" />
        <pixel index="42" colour="0xFF65FF74" />
        <pixel index="44" colour="0xFF65FF74" />
        <pixel index="45" colour="0xFF65FF74" />
        <pixel index="48" colour="0xFF65FF74" />
        <pixel index="56" colour="0xFF65FF74" />
        <pixel index="59" colour="0xFF65FF74" />
        <pixel index="64" colour="0xFF65FF74" />
        <pixel index="70" colour="0xFF65FF74" />
        <pixel index="80" colour="0xFFFB3939" />
        <pixel index="81" colour="0xFFFB3939" />
        <pixel index="82" colour="0xFFFB3939" />
        <pixel index="83" colour="0xFFFB3939" />
        <pixel index="95" colour="0xFFFB3939" />
        <pixel index="103" colour="0xFFFFEE4F" />
        <pixel index="105" colour="0xFFFFEE4F" />
        <pixel index="106" colour="0xFFFFEE4F" />
        <pixel index="107" colour="0xFFFFEE4F" />
        <pixel index="110" colour="0xFFFB3939" />
        <pixel index="117" colour="0xFFFFEE4F" />
        <pixel index="118" colour="0xFFFFEE4F" />
        <pixel index="119" colour="0xFFFFEE4F" />
        <pixel index="125" colour="0xFFFB3939" />
        <pixel index="133" colour="0xFFFFEE4F" />
        <pixel index="140" colour="0xFFFB3939" />
        <pixel index="141" colour="0xFFFB3939" />
        <pixel index="142" colour="0xFFFB3939" />
        <pixel index="143" colour="0xFFFB3939" />
        <pixel index="157" colour="0xFF65FF74" />
        <pixel index="172" colour="0xFF65FF74" />
        <pixel index="185" colour="0xFF65FF74" />
        <pixel index="187" colour="0xFF65FF74" />
        <pixel index="189" colour="0xFF65FF74" />
        <pixel index="201" colour="0xFF65FF74" />
        <pixel index="202" colour="0xFF65FF74" />
        <pixel index="203" colour="0xFF65FF74" />
        <pixel index="217" colour="0xFF65FF74" />
    </pixels>
</display>
*/