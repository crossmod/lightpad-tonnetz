# lightpad-tonnetz

ROLI lightpad script for interactively exploring the tonnetz grid.

## Usage
Press the note name in the middle to play the indicated chord. Red note names are major, blue are minor. If there's a yellow dot next to the note name, read it as "flat".

Press the arrows to navigate the tonnetz.

Press the + or - buttons to change octaves.

Press the + and - buttons simultaneously to reset to C major.

To latch the current chord (e.g. if you're playing through an arpeggiator) move your finger off the chord letter before lifting it.
